import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BakeListComponent } from './bake-list.component';

describe('BakeListComponent', () => {
  let component: BakeListComponent;
  let fixture: ComponentFixture<BakeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BakeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BakeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
