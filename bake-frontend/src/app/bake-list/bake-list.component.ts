import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl,Validators} from '@angular/forms';
import { Bake } from '../bake/bake';
import { BakeService } from '../bake/bake.service';
import {MatAccordion} from '@angular/material/expansion';
import {bakeErrorMatcher} from '../bake/bake.component';

@Component({
  selector: 'app-bake-list',
  templateUrl: './bake-list.component.html',
  styleUrls: ['./bake-list.component.css']
})
export class BakeListComponent implements OnInit {
  bakes: Bake[];
  selectedBake: Bake;
  matcher = new bakeErrorMatcher();
  tempFormControl = new FormControl('',[
      Validators.required,
  ]);
  timeFormControl = new FormControl('',[
      Validators.required,
  ]);

  @ViewChild(MatAccordion) accordion: MatAccordion;


  getBakes(): void {
      this.bakeSerivce.getBakes().then(p =>p.subscribe((b: Bake[]) => this.bakes = b) );
  }

  constructor(private bakeSerivce: BakeService ) {}

  ngOnInit(): void {
      this.getBakes();
  }

  add(temp: number, time: string) : void {
      let b = {temperature: Number(temp) , time: time};
      this.bakeSerivce.add(b)
      .subscribe(bake => {this.bakes.push(bake)});
  }

}
