import { Component, OnInit, Input} from '@angular/core';
import {Bake} from '../bake/bake';

@Component({
  selector: 'app-bake-detail',
  templateUrl: './bake-detail.component.html',
  styleUrls: ['./bake-detail.component.css']
})
export class BakeDetailComponent implements OnInit {
  @Input() bake: Bake;

  constructor() { }

  ngOnInit(): void {
  }

}
