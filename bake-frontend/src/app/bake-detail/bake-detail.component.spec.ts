import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BakeDetailComponent } from './bake-detail.component';

describe('BakeDetailComponent', () => {
  let component: BakeDetailComponent;
  let fixture: ComponentFixture<BakeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BakeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BakeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
