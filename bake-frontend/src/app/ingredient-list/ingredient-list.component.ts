import { Component, OnInit,Input } from '@angular/core';
import {IngredientService} from '../ingredient/ingredient.service';
import {Ingredient, InsertableIngredient } from '../ingredient/ingredient';
import {bakeErrorMatcher} from '../bake/bake.component';
import {Validators, FormControl} from '@angular/forms';
import {serviceConfig} from '../services.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent implements OnInit {

  ingredients: Ingredient[];
  matcher = new bakeErrorMatcher();
  @Input() bake_id: number;

  quantityFormControl = new FormControl('',[
      Validators.required,
  ])

  nameFormControl = new FormControl('',[
      Validators.required,
  ])

  constructor(private service: IngredientService) { }

  ngOnInit(): void {
      this.get_ingredients();
  }

  get_ingredients(){
      this.service.get_ingredients_for_bake(this.bake_id)
      .then( (s:Observable<Ingredient[]>) => {
          s.subscribe(i => this.ingredients = i);
      })
  }


  add(n:string,q:string):void {
      let new_ingredient:InsertableIngredient = {bake_id: Number(this.bake_id) ,name:n,quantity:q}
      this.service.add(new_ingredient)
      .subscribe(i => this.ingredients.push(i));
  }

}
