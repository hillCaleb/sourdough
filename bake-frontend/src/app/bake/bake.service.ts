import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Bake, InsertableBake } from './bake';
import {ServicesService,serviceConfig} from '../services.service';

@Injectable({
  providedIn: 'root'
})
export class BakeService {
  bakesUrl = "/api/bake/";
  config: serviceConfig;

  constructor(private http: HttpClient,private service: ServicesService ) {
      this.getServiceConfig();
  }

  getBakes(): Promise<Observable<Bake[]>>{
      return new Promise(resolve =>{
      this.service.get_service("data_service").subscribe(s => {
          let url = "http://" + s.host_name + ":" + s.port + this.bakesUrl;
          resolve(this.http.get<Bake[]>(url));
      });
      });
  }

  getServiceConfig(){
    if(this.config){
      return this.config;
    }else {
      this.service.get_service("data_service").subscribe(s => this.config = s);
    }
  }

  add(tb: InsertableBake):Observable<Bake> {
      console.log(tb);
      return this.http.post<Bake>(this.bakesUrl, tb);
  }
  update(b: Bake): Observable<Bake> {
      return this.http.put<Bake>(this.bakesUrl, b);
  }
}
