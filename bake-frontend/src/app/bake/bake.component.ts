import { Component, OnInit, ViewChild,Input } from '@angular/core';
import { BakeService } from './bake.service';
import { Bake } from './bake';
import {FormControl, FormGroupDirective, NgForm, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import {MatAccordion} from '@angular/material/expansion';
import {ServicesService} from '../services.service';

export class bakeErrorMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null):boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

@Component({
  selector: 'app-bake',
  templateUrl: './bake.component.html',
  styleUrls: ['./bake.component.css']
})
export class BakeComponent implements OnInit {
  @Input() bake: Bake;
  matcher = new bakeErrorMatcher();
  tempFormControl = new FormControl('', [
      Validators.required,
  ]);

  timeFormControl = new FormControl('', [
      Validators.required,
  ]);

  @ViewChild(MatAccordion) accordion: MatAccordion;

  constructor(private bakeSerivce: BakeService) { }

  ngOnInit(): void{}


  update(): void{
      console.log(this.bake);
      this.bakeSerivce.update(this.bake).subscribe();
  }

}
