export interface Bake {
    id: number,
    temperature: number,
    time: string,
    uuid: string
}

export interface InsertableBake {
    temperature: number,
    time: string,
}
