import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServicesService {

  specificServiceUrl = "http://knower:42069/services/";

  constructor(private http: HttpClient) { }

  get_service(service_name: string):Observable<serviceConfig> {
      let url = this.specificServiceUrl + service_name;
      console.log(url);
      return this.http.get<serviceConfig>(url);
  }
}

export interface serviceConfig {
    service_name: string,
    host_name: string,
    port: number
}
