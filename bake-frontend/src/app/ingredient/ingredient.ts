export interface Ingredient{
    id: number,
    bake_id: number,
    name: string,
    quantity: string,
    uuid: string
}

export interface InsertableIngredient {
    bake_id: number,
    name: string,
    quantity: string,
}
