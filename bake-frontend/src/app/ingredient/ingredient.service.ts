import {Observable} from 'rxjs';
import { Injectable } from '@angular/core';
import {InsertableIngredient,Ingredient} from './ingredient';
import {HttpClient} from '@angular/common/http';
import {ServicesService} from '../services.service';

@Injectable({
  providedIn: 'root'
})
export class IngredientService {

  base_url = "/api/ingredient/";
  constructor(private http: HttpClient, private service: ServicesService) { }

  get_ingredients_for_bake(id:number): Promise<Observable<Ingredient[]>>{
      return new Promise(resolve =>{
      this.service.get_service("data_service").subscribe(s => {
          let url = "http://" + s.host_name + ":" + s.port + this.base_url +"bake/" + id;
          resolve(this.http.get<Ingredient[]>(url));
          });
      });
  }

  add(i:InsertableIngredient):Observable<Ingredient>{
      console.log(i);
      return this.http.post<Ingredient>(this.base_url,i)
  }

  
}
