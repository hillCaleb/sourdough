extern crate service_info;
use service_info::{ServiceInfo,ServiceConfirmation};
use std::collections::HashMap;
use std::sync::Mutex;
use actix_web::middleware;
use actix_web::http::header::ACCESS_CONTROL_ALLOW_ORIGIN;
use actix_web::{web,post,get,App,HttpServer,HttpResponse,Error};
use log::info;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    pretty_env_logger::init();
    info!("Starting Server at knower:42069");
    let services = web::Data::new(ServicesHolder{services: Mutex::new(HashMap::new())});
    HttpServer::new(move || {
        App::new().app_data(services.clone())
            .wrap(middleware::DefaultHeaders::new().header(ACCESS_CONTROL_ALLOW_ORIGIN,"*"))
            .service(web::scope("/").configure(scoped_config))
    })
    .workers(2)
    .bind("0.0.0.0:42069")
    .unwrap()
    .run()
    .await
}
struct ServicesHolder {
    services: Mutex<HashMap<String,ServiceInfo>>
}
#[post("/add")]
async fn add(info: web::Json<ServiceInfo>, data: web::Data<ServicesHolder>) -> Result<HttpResponse,Error> {
    info!("add");
    let name: String = info.service_name.clone();
    match data.services.lock() {
        Ok( mut s) => {
            s.insert(name,info.clone());
            Ok(HttpResponse::Ok().json(ServiceConfirmation {success: true}))
            },
        Err(_) => panic!("Poisoned mutex, not sure how to recover")
    }
}

#[get("/services/{service_name}")]
async fn get(service_name: web::Path<String>,data: web::Data<ServicesHolder>) -> Result<HttpResponse,Error> {
    info!("get");
    let name: String = service_name.clone();
    match data.services.lock().unwrap().get(&name){
        Some(info) => Ok(HttpResponse::Ok().json(Some(info))),
        None => Ok(HttpResponse::Ok().json((name,None::<ServiceInfo>)))
    }
}

#[get("/list")]
async fn list(data: web::Data<ServicesHolder>) -> Result<HttpResponse,Error> {
    info!("listing");
    let list: Vec<ServiceInfo> = data.services.lock().unwrap()
              .values()
              .map(|a| a.clone())
              .collect();

    Ok( HttpResponse::Ok().json(list))
}





fn scoped_config(cfg: &mut web::ServiceConfig) {
    cfg.service(add);
    cfg.service(get);
    cfg.service(list);
}
