extern crate reqwest;
extern crate serde;

use std::clone::Clone;
use std::fmt::Display;
use std::fmt::Formatter;
use serde::{Serialize,Deserialize};

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}

#[derive(Serialize,Deserialize,Clone)]
pub struct ServiceInfo{
    pub service_name: String,
    pub host_name: String,
    pub port: i32
}

#[derive(Serialize,Deserialize)]
pub struct ServiceConfirmation {
    pub success: bool
}
impl Display for ServiceConfirmation {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f,"confirmed status: {}", self.success)
    }
}


pub fn register(info: ServiceInfo) -> Result<ServiceConfirmation,()>{
    let client = reqwest::blocking::Client::new();
    let r: ServiceConfirmation = client.post("http://knower:42069/add")
        .json(&info)
        .send().unwrap()
        .json().expect("wrong thing came back");
    Ok(r)
}
