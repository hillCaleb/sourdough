table! {
    bakes (id) {
        id -> Integer,
        temperature -> Integer,
        time -> Text,
        uuid -> Text,
    }
}

table! {
    ingredients (id) {
        id -> Integer,
        bake_id -> Integer,
        name -> Text,
        quantity -> Text,
        uuid -> Text,
    }
}

allow_tables_to_appear_in_same_query!(
    bakes,
    ingredients,
);
