#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate reqwest;
extern crate pretty_env_logger;
extern crate log;
extern crate service_info;

pub mod schema;
pub mod bake;
pub mod ingredient;
/*
use bake::{Bake,NewBake};
use ingredient::{Ingredient,NewIngredient};
use schema::bake::dsl::*;

*/
use service_info::ServiceInfo;
use log::info;
use diesel::sqlite::SqliteConnection;
use diesel::r2d2::{self,ConnectionManager};
use dotenv::dotenv;
use std::env;
use actix_web::{web,App,HttpServer};
use actix_session::CookieSession;
use actix_files::Files;
use actix_web::http::header::ACCESS_CONTROL_ALLOW_ORIGIN;
use actix_web::middleware;

type Connection = SqliteConnection;
type DbPool =  r2d2::Pool<ConnectionManager<Connection>>;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    pretty_env_logger::init();
    let info = ServiceInfo {
        service_name:"data_service".to_string(),
        host_name:"localhost".to_string(),
        port: 8088
    };
    match service_info::register(info) {
        Ok(a) => info!("ok: {}",a),
        _ => info!("error")
    };


    let mananger = ConnectionManager::<Connection>::new(database_url());
    info!("Building pool");
    let pool = r2d2::Pool::builder()
        .build(mananger)
        .expect("pool was not built");
    info!("built pool");

    info!("Starting Server at port 8088");
    HttpServer::new(move || {
        App::new().data(pool.clone())
        .wrap(CookieSession::signed(&[0;32]))
        .wrap(middleware::DefaultHeaders::new().header(ACCESS_CONTROL_ALLOW_ORIGIN,"*"))
        .service(web::scope("/api")
            .service(web::scope("/bake") .configure(bake::scoped_config))
            .service(web::scope("/ingredient").configure(ingredient::scoped_config))
        ).service(Files::new("/","./bake-frontend/dist/bake-frontend").index_file("index.html"))
    })
    .bind("0.0.0.0:8088")
    .unwrap()
    .run()
    .await
}

fn database_url() -> String {
    dotenv().ok();
    env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set")
}

