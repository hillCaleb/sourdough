use crate::schema::ingredients;
use crate::diesel::{RunQueryDsl,QueryDsl};
use actix_web::{web,get,post,delete,HttpResponse,Error};
use crate::{DbPool,Connection};
use log::{info,error};
use serde::{Serialize,Deserialize};
use diesel::prelude::*;
use uuid::Uuid;


#[derive(Queryable,Serialize)]
pub struct Ingredient {
    pub id: i32,
    pub bake_id: i32,
    pub name: String,
    pub quantity: String,
    pub uuid: String
}

#[derive(Insertable,Serialize,Deserialize)]
#[table_name="ingredients"]
pub struct NewIngredient {
    pub bake_id: i32,
    pub name: String,
    pub quantity: String,
    pub uuid: Option<String>
}


pub fn scoped_config(cfg: &mut web::ServiceConfig) {
    cfg.service(get);
    cfg.service(get_single);
    cfg.service(add);
    cfg.service(get_by_bake);
}

#[post("/")]
async fn add(i: web::Json<NewIngredient>,pool: web::Data<DbPool>) -> Result<HttpResponse,Error> {
    let uuid = Uuid::new_v4();
    let new_ingredient = NewIngredient{
        bake_id: i.bake_id.clone(),
        name: i.name.clone(),
        quantity: i.quantity.clone(),
        uuid: Some(uuid.to_string())
    };
    info!("Saving new Ingredient {}",serde_json::to_string(&new_ingredient).unwrap());
    let connection = pool.get().expect("couldn't get db connection'");
    web::block(move || insert(new_ingredient,&connection)).await?;
    let connection = pool.get().expect("couldn't get db connection'");
    let b = web::block(move || get_by_uuid(uuid.to_string(),&connection)).await?;
    Ok(HttpResponse::Ok().json(b))
}


#[get("/")]
async fn get(pool: web::Data<DbPool>) -> Result<HttpResponse,Error>{
    let db = pool.get().expect("could not get connection");
    match web::block(move || get_all(&db)).await {
        Ok(i) => Ok(HttpResponse::Ok().json(i)),
        Err(e) => Err(Error::from(e)),
    }
}

#[get("/{local_id}")]
async fn get_single(local_id: web::Path<i32>, pool: web::Data<DbPool>) -> Result<HttpResponse,Error>{
    info!("getting ingredient with id: {}", local_id);
    let id_copy = local_id.clone();
    let connection = pool.get().expect("couldn't get db connection");
    let b = web::block(move || get_by_id(*local_id,&connection)).await
        .map_err(|e|{
            error!("Error: {}",e);
            HttpResponse::InternalServerError().finish()
        }).ok();
    if let Some(b) = b {
        Ok(HttpResponse::Ok().json(b))
    } else {
        let res = HttpResponse::NotFound()
            .body(format!("No ingredient found with id: {}",id_copy));
        Ok(res)
    }
}


#[delete("/{id}")]
async fn remove(id: web::Path<i32>,pool: web::Data<DbPool>)-> Result<HttpResponse,Error>{
    let connection = pool.get().expect("couldn't get db connection");
    let removed = web::block(move|| delete_ingredient(*id, &connection)).await?;
    Ok(HttpResponse::Ok().json(removed))
}

#[get("/bake/{bake_id}")]
async fn get_by_bake(bake_id:web::Path<i32>,pool: web::Data<DbPool>)
    -> Result<HttpResponse,Error>
{
    info!("getting ingredients for bake {}",bake_id);
    let connection = pool.get().expect("couldn't get db connection");
    let ingredient_list = web::block(move || get_by_bake_id(*bake_id,&connection)).await?;
    Ok(HttpResponse::Ok().json(ingredient_list))
}

fn get_by_bake_id(local_id: i32, con: &Connection) -> Result<Vec<Ingredient>,diesel::result::Error> {
    use crate::schema::ingredients::dsl::*;
    ingredients.filter(bake_id.eq(local_id)).load::<Ingredient>(con)
}


fn insert(new_ingredient: NewIngredient, con: &Connection) -> Result<(),diesel::result::Error>{
    info!("inserting new ingredient:{}",serde_json::to_string(&new_ingredient).unwrap());
    use crate::schema::ingredients::dsl::*;
    diesel::insert_into(ingredients).values(&new_ingredient).execute(con)?;
    Ok(())
}

fn get_by_id(local_id: i32, con: &Connection) -> Result<Option<Ingredient>,diesel::result::Error> {
    info!("getting Ingredient with ID: {}",local_id);
    use crate::schema::ingredients::dsl::*;
    let i = ingredients.filter(id.eq(local_id)).first::<Ingredient>(con).optional()?;
    Ok(i)
}

fn get_by_uuid(local_uuid: String, con: &Connection) -> Result<Option<Ingredient>,diesel::result::Error> {
    info!("getting Ingredient with uuid: {}",local_uuid);
    use crate::schema::ingredients::dsl::*;
    let i = ingredients.filter(uuid.eq(local_uuid)).first::<Ingredient>(con).optional()?;
    Ok(i)
}

fn get_all(con: &Connection) -> Result<Vec<Ingredient>, diesel::result::Error> {
    info!("getting all Ingredients ");
    use crate::schema::ingredients::dsl::*;
    ingredients.load::<Ingredient>(con)
}

fn delete_ingredient(local_id: i32, con: &Connection) -> Result<Option<usize>,diesel::result::Error> {
    use crate::schema::ingredients::dsl::*;
    let num_deleted = diesel::delete(ingredients).filter(id.eq(local_id)).execute(con)?;
    Ok(Some(num_deleted))
}
