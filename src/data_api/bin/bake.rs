use crate::diesel::{RunQueryDsl,QueryDsl};
use crate::schema::*;
use serde::{Serialize,Deserialize};
use diesel::prelude::*;
use crate::{DbPool,Connection};
use uuid::Uuid;
use actix_web::{web,post,get,delete,put,HttpResponse,Error};
use log::{info,debug,error};

pub fn scoped_config(cfg: &mut web::ServiceConfig) {
    cfg.service(add);
    cfg.service(get);
    cfg.service(get_single);
    cfg.service(remove_bake);
    cfg.service(update);
}

#[post("/")]
async fn add(pool: web::Data<DbPool>, nb: web::Json<NewBake> ) -> Result<HttpResponse,Error> {
    let uuid = Uuid::new_v4();
    let new_bake = NewBake{temperature:nb.temperature , time:nb.time.clone() ,uuid: Some(uuid.to_string()) };
    info!("Saving new Bake {}",serde_json::to_string(&new_bake).unwrap());
    let connection = pool.get().expect("couldn't get db connection'");
    web::block(move || insert_bake(new_bake,&connection)).await?;
    let connection = pool.get().expect("couldn't get db connection'");
    let b = web::block(move || get_by_uuid(uuid.to_string(),&connection)).await?;
    Ok(HttpResponse::Ok().json(b))
}

#[put("/")]
async fn update(pool: web::Data<DbPool>, b: web::Json<Bake>) -> Result<HttpResponse,Error> {
    match web::block(
        move || update_bake(b.clone(),&pool.get().expect("couldn't get db connection"))
        ).await {
        Ok(_) => Ok(HttpResponse::Ok().finish()),
        Err(e) => Err(Error::from(e)) ,
    }
}

#[get("/")]
async fn get(pool: web::Data<DbPool>) -> Result<HttpResponse,Error> {
    info!("getting all bakes");
    let connection = pool.get().expect("couldn't get db connection'");
    match web::block(move || get_all_bakes(&connection)).await {
        Ok(d) => Ok(HttpResponse::Ok().json(d)),
        Err(e) => Err(Error::from(e)),
    }
}

#[get("/{local_id}")]
async fn get_single(pool: web::Data<DbPool>,local_id: web::Path<i32> ) -> Result<HttpResponse,Error> {
    info!("getting bake with id: {}", local_id);
    let id_copy = local_id.clone();
    let connection = pool.get().expect("couldn't get db connection");
    let b = web::block(move || get_single_bake(*local_id,&connection)).await
        .map_err(|e|{
            error!("Error: {}",e);
            HttpResponse::InternalServerError().finish()
        }).ok();
    if let Some(b) = b {
        Ok(HttpResponse::Ok().json(b))
    } else {
        let res = HttpResponse::NotFound()
            .body(format!("No bake found with id: {}",id_copy));
        Ok(res)
    }
}

#[delete("/{id}")]
async fn remove_bake(id: web::Path<i32>, pool: web::Data<DbPool>) -> Result<HttpResponse,Error> {
    let connection = pool.get().expect("couldn't get db connection");
    let removed = web::block(move|| delete_bake(*id, &connection)).await?;
    Ok(HttpResponse::Ok().json(removed))
}

fn insert_bake(b: NewBake, con: &Connection) -> Result<(),diesel::result::Error> {
    debug!("inserting bake {}",serde_json::to_string(&b).unwrap());
    use crate::schema::bakes::dsl::*;
    diesel::insert_into(bakes).values(&b).execute(con)?;
    debug!("inserted bake");
    Ok(())
}

fn update_bake(b: Bake, con: &Connection) -> Result<(),diesel::result::Error> {
    debug!("updating bake {}",b.id);
    diesel::update(&b).set(&b).execute(con)?;
    Ok(())
}

fn get_all_bakes(con: &Connection) -> Result<Vec<Bake>,diesel::result::Error> {
    use crate::schema::bakes::dsl::*;
    bakes.load::<Bake>(con)
}

fn get_single_bake(server_id: i32, con: &Connection) -> Result<Option<Bake>,diesel::result::Error> {
    use crate::schema::bakes::dsl::*;
    let b = bakes.filter(id.eq(server_id)).first::<Bake>(con).optional()?;
    Ok(b)
}

fn get_by_uuid(local_uuid: String,con: &Connection)  -> Result<Option<Bake>,diesel::result::Error> {
    use crate::schema::bakes::dsl::*;
    let b = bakes.filter(uuid.eq(local_uuid)).first::<Bake>(con).optional()?;
    Ok(b)
}

fn delete_bake(id_local: i32,con: &Connection) -> Result<Option<usize>,diesel::result::Error> {
    use crate::schema::bakes::dsl::*;
    let num_deleted = diesel::delete(bakes).filter(id.eq(id_local)).execute(con)?;
    Ok(Some(num_deleted))
}

#[derive(Queryable,Clone,Serialize,Deserialize,AsChangeset,Identifiable)]
#[table_name="bakes"]
pub struct Bake {
    pub id: i32,
    pub temperature:i32,
    pub time: String,
    pub uuid: String
}

#[derive(Insertable,Serialize,Deserialize)]
#[table_name="bakes"]
pub struct NewBake {
    pub temperature:i32,
    pub time: String,
    pub uuid: Option<String>,
}


