extern crate service_info;
use service_info::ServiceInfo;
use actix_web::{App,HttpServer};
use actix_files::Files;
use actix_web::http::header::ACCESS_CONTROL_ALLOW_ORIGIN;
use actix_web::middleware;
use log::info;


#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    pretty_env_logger::init();
    let info = ServiceInfo {
        service_name:"angular_service".to_string(),
        host_name:"localhost".to_string(),
        port: 8080
    };
    match service_info::register(info) {
        Ok(a) => info!("ok: {}",a),
        _ => info!("error")
    };
    info!("Starting Server at port 8080");
    HttpServer::new(move || {
        App::new()
            .wrap(middleware::DefaultHeaders::new().header(ACCESS_CONTROL_ALLOW_ORIGIN,"*"))
            .service(
            Files::new("/","./bake-frontend/dist/bake-frontend").index_file("index.html"))
    })
    .bind("0.0.0.0:8080")
    .unwrap()
    .run()
    .await
}
