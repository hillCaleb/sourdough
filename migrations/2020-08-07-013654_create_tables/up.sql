-- Your SQL goes here
CREATE TABLE bakes (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
    temperature INTEGER NOT NULL,
    time TEXT NOT NULL
);


CREATE TABLE ingredients (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    bake_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    quantity TEXT NOT NULL,
    FOREIGN KEY(bake_id) REFERENCES Bake(id)
);
