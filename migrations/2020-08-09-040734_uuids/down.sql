-- This file should undo anything in `up.sql`
PRAGMA foreign_keys=off;

CREATE TABLE IF NOT EXISTS temp_bake(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,
    temperature INTEGER NOT NULL,
    time TEXT NOT NULL
);

INSERT INTO temp_bake(id,temperature,time)
SELECT id, temperature,time
from bake;

DROP TABLE bake;

ALTER TABLE temp_bake RENAME TO bake;


CREATE TABLE IF NOT EXISTS temp_ingredient(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    bake_id INTEGER NOT NULL,
    name TEXT NOT NULL,
    quantity TEXT NOT NULL
);

INSERT INTO temp_ingredient(id,bake_id,name,quantity)
SELECT id,bake_id,name,quantity
from ingredient;

DROP TABLE ingredient;

ALTER TABLE temp_ingredient RENAME TO ingredient;


PRAGMA foreign_keys=on;
